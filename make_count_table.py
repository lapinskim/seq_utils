#!/usr/bin/env python3
# coding: utf-8

import argparse
import concurrent.futures
import logging
import os
import subprocess
import sys

desc = """
Create table with samtools view counts for different
flags and filtering options.
"""

logger_format = (
    "%(asctime)s:%(levelname)s:%(name)s:\
%(threadName)s(%(process)s):%(message)s"
)
logging.basicConfig(
    format=logger_format,
    level=logging.WARNING,
    datefmt="%H:%M:%S",
)

paired = 1
proper_pair = 2
unmapped = 4
first_in_pair = 64
not_primary = 256
duplicate = 1024
supplementary = 2048


class Flags:
    def __init__(
            self,
            include=0,
            exclude=0,
            quality=0,
            key="counts"
    ):
        self.include = include
        self.exclude = exclude
        self.quality = quality
        self.key = key
        self.description = "included: {o.include}, excluded: {o.exclude}, \
quality: {o.quality}".format(o=self)


def run_count(
        bam_file_name,
        include_flag=0,
        exclude_flag=0,
        quality=0,
):
    include_arg = "-f{}".format(include_flag)
    exclude_arg = "-F{}".format(exclude_flag)
    quality_arg = "-q{}".format(quality)
    cmd = "samtools"
    args = [
        cmd,
        "view",
        "-c",
        include_arg,
        exclude_arg,
        quality_arg,
        bam_file_name,
    ]
    p = subprocess.Popen(
        args,
        stdout=subprocess.PIPE, stderr=subprocess.PIPE
    )
    out, errs = p.communicate()
    if errs:
        logging.error(errs.decode("utf-8"))
    if p.returncode != 0:
        raise subprocess.CalledProcessError(p.returncode, cmd)

    return out.decode("utf-8").strip("\n")


def write_table(stats, result, sep="\t"):
    header = """# plot_type: 'table'
# section_name: 'Alignment summary and filtering report'
# description: 'Table containing the sumary statictics of different alignment \
characteristics useful for quantifying the alignments during filtering steps. \
Hover over the columns to see description of included and excluded flags.'
# pconfig:
#     namespace: 'Samtools'
# headers:
"""
    header_line = "#     {stat}:\n#         title: '{stat}'\n#         \
description: '{value.description}'\n#         format: '{{:,.0f}}'\n\
#         placement: {i}\n#         shared_key: '{value.key}'\n"
    Filtered = Flags()
    Filtered.description = "Mapped - Remaining counts"
    stats.insert(-1, ("Filtered", Filtered))
    columns = ["{" + s[0] + "}" for s in stats]
    table_line = (sep.join(["{Sample}"] + columns) + "\n")
    sys.stdout.write(header)
    for i, (stat, value) in enumerate(stats):
        sys.stdout.write(header_line.format(i=i, stat=stat, value=value))
    sys.stdout.write(table_line.format(
        **{s[0]: s[0] for s in stats},
        Sample="Sample",
    ))
    for f in result:
        sys.stdout.write(table_line.format(
            **{s: v.result() for s, v in result[f].items()},
            Sample=os.path.basename(f).split(".")[0],
            Filtered=(int(result[f]["Mapped"].result())
                      - int(result[f]["Remaining"].result())),
        ))


def main(bams, stats, sep="\t", workers=None):
    with concurrent.futures.ProcessPoolExecutor(
            max_workers=workers,
    ) as executor:
        result = {}
        for bam in bams:
            result[bam] = {s: executor.submit(run_count,
                                              bam,
                                              v.include,
                                              v.exclude,
                                              v.quality)
                           for s, v in stats}
        write_table(stats, result, sep=sep)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument(
        "bams",
        metavar="BAM",
        help="BAM file to process",
        type=str,
        nargs="+",
    )
    parser.add_argument(
        "-p",
        "--pair-end",
        help="Process the alignments as fragments, \
showing stats for the first in pair.",
        action='store_true',
    )
    parser.add_argument(
        "-c",
        "--cpus",
        type=int,
        default=None,
        help="Number of cpus to utilize.",
    )
    parser.add_argument(
        "-i",
        "--included",
        help="Included flags for filtering.",
        type=int,
        default=0,
    )
    parser.add_argument(
        "-x",
        "--excluded",
        help="Included flags for filtering.",
        type=int,
        default=0,
    )
    parser.add_argument(
        "-q",
        "--quality",
        help="Minimum quality for filtering.",
        type=int,
        default=0,
    )
    parser.add_argument(
        "-s",
        "--separator",
        help="Table separator character",
        type=str,
        default="\t",
    )
    args = parser.parse_args()

    fragments = 0
    pe_to_frag = 0
    if args.pair_end:
        fragments = paired + proper_pair + first_in_pair
        pe_to_frag = first_in_pair

    stat_types = [
        ("Unmapped", Flags(unmapped, key="reads")),
        ("Supplementary", Flags(supplementary, key="reads")),
        ("Mapped", Flags(fragments, unmapped)),
        ("Secondary", Flags(fragments + not_primary)),
        ("Duplicates", Flags(fragments + duplicate)),
        ("Remaining", Flags(
            args.included + pe_to_frag,
            args.excluded if args.excluded & 4 else args.excluded + 4,
            args.quality,
        )),
    ]
    if args.pair_end:
        stat_types.insert(1, (
            "Improper",
            Flags(paired, proper_pair + unmapped, key="reads"),
        ))

    main(args.bams, stat_types, sep=args.separator, workers=args.cpus)
