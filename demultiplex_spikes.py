#!/usr/bin/env python3
"""Demultiplex reads by barcodes falling into specified reference contigs

SW scoring parameters:
Match score: {MATCH_SCORE}
Mismatch score: {MISMATCH_SCORE}
Open gap score: {OPEN_GAP_SCORE}
Extend gap score: {EXTEND_GAP_SCORE}"""


import argparse
import logging
import os
import time
from Bio import Align
import matplotlib.pyplot as plt
import pandas as pd
import pysam


LOGGER_FORMAT = "%(asctime)s:%(levelname)s:%(name)s:\
%(threadName)s(%(process)s): %(message)s"

MATCH_SCORE = 1.0
MISMATCH_SCORE = -0.33
OPEN_GAP_SCORE = -1.0
EXTEND_GAP_SCORE = -0.33


def align_to_read(
    read: pysam.AlignedSegment,
    barcodes: dict[str, str],
    scan_bases: int,
    mode: str = "local",
    match_score: float = 1,
    mismatch_score: float = -0.33,
    open_gap_score: float = -1,
    extend_gap_score: float = -0.33,
) -> list[tuple[float, str]]:
    """Perform SW alignment to read"""
    aligner = Align.PairwiseAligner()
    aligner.mode = mode
    aligner.match_score = match_score
    aligner.mismatch_score = mismatch_score
    aligner.open_gap_score = open_gap_score
    aligner.extend_gap_score = extend_gap_score
    return [
        (aligner.align(read.query_sequence[:scan_bases], barcode).score, b_name)
        for b_name, barcode in barcodes.items()
    ]


def demultiplex(
    read: pysam.AlignedSegment,
    barcodes: dict[str, str],
    scan_bases: int,
    cutoff,
    mode: str = "local",
    match_score: float = MATCH_SCORE,
    mismatch_score: float = MISMATCH_SCORE,
    open_gap_score: float = OPEN_GAP_SCORE,
    extend_gap_score: float = EXTEND_GAP_SCORE,
) -> tuple[pysam.AlignedSegment, dict]:
    """Find best alignment score above the specified CUTOFF value"""
    alignment_results = align_to_read(
        read,
        barcodes,
        scan_bases,
        mode,
        match_score,
        mismatch_score,
        open_gap_score,
        extend_gap_score,
    )
    scores, barcodes = list(zip(*alignment_results))
    best_score = max(scores)
    for i, score in enumerate(scores):
        if score == best_score:
            best_bc = barcodes[i]
            break
    assigned = "undefined"
    if best_score > cutoff:
        assigned = best_bc
    return (
        read,
        {
            "name": read.query_name,
            "score": scores,
            "barcode": barcodes,
            "assigned": assigned,
        },
    )


def split_reads(
    demultiplex_result: tuple[pysam.AlignedSegment, list[dict]],
    file_pool: dict,
) -> list[dict]:
    """Write read to proper file if the file dict is not empty"""
    read, result = demultiplex_result
    if file_pool:
        assignment = result["assigned"]
        file_pool[assignment].write(read)
    return result


def plot_hist(
    scores: list,
    barcode: str,
    axes: plt.Axes,
    cutoff: int,
    bins: int = 50,
    linewidth: float = 2,
    histtype: str = "step",
    alpha: float = 0.75,
) -> tuple:
    """Plot histogram on given AXES"""
    n_cutoff = len([e for e in scores if e > cutoff])
    return axes.hist(
        scores,
        bins=bins,
        density=False,
        linewidth=linewidth,
        histtype=histtype,
        alpha=alpha,
        label=f"{barcode}: {n_cutoff}",
    )


def plot_table(
    table: pd.DataFrame,
    contig: str,
    cutoff: int,
    prefix: str = "",
    file_format: str = "png",
    bins: int = 50,
    dpi: int = 300,
) -> None:
    """Plot histograms for each barcode from the given TABLE"""
    fig, axes = plt.subplots()
    for group in table.groupby("barcode"):
        barcode = group[0]
        scores = group[1].score
        plot_hist(scores, barcode, axes, cutoff, bins)

    axes.axvline(
        cutoff,
        ymin=0,
        ymax=1,
        color="r",
        linestyle="dashed",
        label="Cutoff",
    )
    axes.legend(prop={"size": 10})
    axes.set_xlabel("Scores")
    axes.set_ylabel("Counts")
    axes.set_title("Contig: " + contig)

    file_name = f"hist_{contig}.{file_format}"
    out_path = os.path.join(prefix, file_name)
    fig.savefig(out_path, dpi=dpi)
    plt.close(fig=fig)


def plot_summary(
    data: pd.DataFrame,
    contig: str,
    prefix: str = "./",
    file_format: str = "png",
    dpi: int = 300,
) -> tuple:
    """Plot summary barplot from the demultiplexing results"""
    data["alignment_number"] = data.index
    summary = data.groupby("assigned")["alignment_number"].nunique()
    fig, axes = plt.subplots()
    bars = axes.bar(summary.index, summary)
    axes.bar_label(bars)
    axes.set_xlabel("Assigned")
    axes.set_ylabel("Counts")
    axes.set_title("Contig: " + contig)
    file_name = f"bar_{contig}.{file_format}"
    out_path = os.path.join(prefix, file_name)
    fig.savefig(out_path, dpi=dpi)
    plt.close(fig=fig)
    return bars


def process_contig(
    contig: str,
    bam: pysam.AlignmentFile,
    barcodes: dict[str, str],
    out_dir: str,
    scan_bases: int = 150,
    cutoff: int = 12,
    bins: int = 50,
    f_format: str = "png",
    dry_run: bool = False,
) -> pd.DataFrame:
    """Main function for demultiplexing reads in BAM falling into a given CONTIG"""
    file_pool = {}
    if not dry_run:
        logging.info("Opening output files.")
        file_pool = {
            barcode: pysam.AlignmentFile(
                os.path.join(out_dir, f"{contig}_{barcode}.bam"),
                "wb",
                template=bam,
            )
            for barcode in list(barcodes.keys()) + ["undefined"]
        }

    logging.info("Demultiplexing.")
    table = pd.DataFrame.from_records(
        map(
            lambda result: split_reads(
                result,
                file_pool,
            ),
            map(
                lambda read: demultiplex(read, barcodes, scan_bases, cutoff),
                bam.fetch(contig),
            ),
        )
    ).explode(["score", "barcode"])

    logging.info("Plotting histogram.")
    plot_table(
        table,
        contig,
        cutoff,
        prefix=out_dir,
        file_format=f_format,
        bins=bins,
    )

    logging.info("Plotting summary barplot.")
    plot_summary(
        table,
        contig,
        out_dir,
    )

    for barcode in file_pool:
        file_pool[barcode].close()
    logging.info("Demultiplexing of contig: %s successful.", contig)

    return table


def parse_fasta(fasta_path: str) -> dict[str, str]:
    """Parse fasta file"""
    result = {}
    with open(fasta_path, "r", encoding="utf8") as fasta:
        for line in fasta:
            line = line.rstrip()
            if line.startswith(">"):
                name = line.lstrip(">").split()[0]
                assert name not in result
                result[name] = ""
            else:
                assert name and name in result
                result[name] += line
    return result


if __name__ == "__main__":

    class CustomFormatter(
        argparse.RawDescriptionHelpFormatter, argparse.ArgumentDefaultsHelpFormatter
    ):
        """Allow formatted description and default values in the help strings"""

    parser = argparse.ArgumentParser(
        description=__doc__.format(
            MATCH_SCORE=MATCH_SCORE,
            MISMATCH_SCORE=MISMATCH_SCORE,
            OPEN_GAP_SCORE=OPEN_GAP_SCORE,
            EXTEND_GAP_SCORE=EXTEND_GAP_SCORE,
        ),
        formatter_class=CustomFormatter,
    )
    parser.add_argument("bam", type=str, help="Bam file.")
    parser.add_argument(
        "-b", "--barcodes", help="Fasta file with barcodes.", type=str, required=True
    )
    parser.add_argument(
        "-c",
        "--contigs",
        help="Reference contigs to search in.",
        required=True,
        type=str,
        nargs="+",
    )
    parser.add_argument(
        "-p", "--prefix", help="Output directory prefix.", type=str, default="./"
    )
    parser.add_argument(
        "-s", "--scan_bases", help="Number of 5' bases to scan.", type=int, default=150
    )
    parser.add_argument(
        "-u",
        "--cutoff",
        help="Alignment scoring threshold to consider while demultiplexing.",
        type=int,
        default=12,
    )
    parser.add_argument(
        "-n",
        "--bins",
        help="Number of bins to plot on the histogram.",
        type=int,
        default=50,
    )
    parser.add_argument(
        "-f", "--format", help="File format for the plots.", type=str, default="png"
    )
    parser.add_argument(
        "-d",
        "--dry-run",
        help="Do not write the reads to the output files.",
        action="store_true",
    )
    parser.add_argument(
        "-v", "--verbose", help="Output verbose log messages.", action="store_true"
    )

    start_time = time.time()

    args = parser.parse_args()

    LOG_LEVEL = logging.WARNING
    if args.verbose:
        LOG_LEVEL = logging.INFO
    logging.basicConfig(
        format=LOGGER_FORMAT,
        level=LOG_LEVEL,
        datefmt="%H:%M:%S",
    )

    bam_file = pysam.AlignmentFile(args.bam, "rb")
    barcode_dict = parse_fasta(args.barcodes)

    contig_tables = []
    for ctg in args.contigs:
        logging.info("Processing contig: %s.", ctg)
        c_table = process_contig(
            contig=ctg,
            bam=bam_file,
            barcodes=barcode_dict,
            out_dir=args.prefix,
            scan_bases=args.scan_bases,
            cutoff=args.cutoff,
            bins=args.bins,
            f_format=args.format,
            dry_run=args.dry_run,
        )
        c_table["contig"] = ctg
        contig_tables.append(c_table)

    bam_file.close()

    logging.info("Writing table to file.")
    demultiplex_table = pd.concat(contig_tables, axis=0, ignore_index=True)
    demultiplex_table.to_csv(
        os.path.join(args.prefix, "demultiplex_table.tsv"),
        sep="\t",
        header=True,
        index=False,
    )

    end_time = time.time()

    logging.info("Done. Elapsed: %s s.", end_time - start_time)
