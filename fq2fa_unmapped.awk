#!/usr/bin/env -S awk -f

BEGIN {
    OFS="\n";
    # Define number of FASTA records to return
    if (!n) {
        n=100;
    }
    # Initiate the counter
    records=0;
}
{
    if ((NR + 3) % 4 == 0 && $0 ~ /^@.+$/) {
        # Extract the read name
        name=substr($0,2);
        # If the aligner added a flag record to the read name get that too
        if (NF > 1) {
            flag=$(NF);
        }
    } else {
        # Every fourth line with the sequence
        if ((NR + 2) % 4 == 0) {
            # If the extracted flag does not indicate
            # that one from the pair mapped
            if (flag !~ /^(01|10)$/) {
                # The sequence does not contain undefined nucleotides
                if ($0 !~ /N/) {
                    print ">" name, $0;
                    ++records;
                    if (records == n) {
                        exit;
                    }
                }
            }
        }
    }
}
