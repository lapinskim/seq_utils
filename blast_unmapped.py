#!/usr/bin/env python3

import os
import sys
import argparse

import matplotlib.pyplot as plt

from Bio.Blast import NCBIWWW
from Bio.Blast import NCBIXML


def get_name(path):
    name = path.strip("<>").rsplit("/", maxsplit=1)
    if len(name) > 1:
        name = name[1].split(".", maxsplit=1)[0]
    else:
        name = name[0].split(".", maxsplit=1)[0]
    return name


def run_blast(query, sample_name):
    print("Processing " + sample_name + ".", file=sys.stderr)

    blast_file = sample_name + ".blast.xml"
    if not os.path.exists(blast_file):
        print("Running Web BLAST. Please wait.", file=sys.stderr)
        result_handle = NCBIWWW.qblast("blastn", "nt", query,
                                       expect=100,
                                       filter="none",
                                       megablast="FALSE",
                                       word_size=11,
                                       )
        with open(blast_file, "w") as blast_results:
            blast_results.write(result_handle.read())
        result_handle.close()
    else:
        print("Results file found.", file=sys.stderr)
    return blast_file


def clean_up(results_xml):
    """Workaround for the messy NCBI API Response"""

    messy_responses = {"CREATE_VIEW\n", "CREATE_VIEW\n\n\n\n"}

    clean_xml = results_xml.split(".")[0] + ".clean.xml"
    with open(results_xml, "r") as dirty_file:
        with open(clean_xml, "w") as clean_file:
            for line in dirty_file:
                if line not in messy_responses:
                    clean_file.write(line)
    return clean_xml


def parse_results(results_path, identity_threshold):
    print("Parsing results.", file=sys.stderr)
    results_file = open(results_path, "r")
    blast_records = NCBIXML.parse(results_file)

    n_records = 0
    species = {}
    for record in blast_records:
        n_records += 1
        query_length = record.query_length
        for alignment in record.alignments:
            sp_name = alignment.hit_def.split(" ", maxsplit=1)[0]
            for hsp in alignment.hsps:
                if hsp.identities / query_length > identity_threshold:
                    species[sp_name] = species.get(sp_name, 0) + 1
                    break
            break
    results_file.close()
    return n_records, species


def count_unident(total_number, species_count):
    labels = ""
    counts = []
    if species_count:
        labels, counts = zip(*species_count.items())
    unident = total_number - sum(counts)
    counts = list(counts)
    if unident:
        labels = list(labels) + ["unidentified"]
        counts.append(unident)
    return labels, counts


def draw_pie(name, labels, counts):
    legend_labels = [label + ": " + str(count) for label, count in zip(labels, counts)]

    fig, ax = plt.subplots(figsize=(6, 3), subplot_kw=dict(aspect="equal"))
    chart = ax.pie(counts, labels=labels, autopct="%1.1f%%")
    ax.axis("equal")
    ax.legend(chart[0], legend_labels,
              title="Species [count]",
              loc="center right",
              bbox_to_anchor=(1, 0.5),
              bbox_transform=fig.transFigure,
              )
    plt.subplots_adjust(left=0.0, bottom=0.1, right=0.7)
    suptitle = plt.suptitle(name + "\nSpecies fraction in unmapped reads", fontsize=10)

    plt.savefig(name + ".pdf")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Run NCBI web BLAST against supplied reads, extract species count."
    )
    parser.add_argument("query_fasta", type=argparse.FileType("r"), default=sys.stdin,
                        help="Input file in FASTA format.")
    parser.add_argument("-i", "--identity", type=float, default=0.75,
                        help="BLAST identity threshold for species inclusion.")
    parser.add_argument("-n", "--name",
                        help="Optional output file and plot name.")
    args = parser.parse_args()

    name = args.name
    if not name:
        name = get_name(args.query_fasta.name)

    with args.query_fasta as query_file:
        query = query_file.read()
        if not query:
            print("Query is empty.", file=sys.stderr)
            sys.exit(0)
        n_queries, species_count = parse_results(clean_up(run_blast(query, name)),
                                                 args.identity,
                                                 )
    labels, counts = count_unident(n_queries, species_count)
    draw_pie(name, labels, counts)
    for sp_name, count in species_count.items():
        print(sp_name + "\t" + str(count), file=sys.stdout)
