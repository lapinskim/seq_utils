#!/usr/bin/env python3
# coding: utf-8
"""Extract features in BED format from the GTF file."""

import argparse
import gzip
import logging
from typing import Dict, Iterable, List, Optional, Set, TextIO, Tuple, Type, TypeVar
import sys

AnnType = TypeVar("AnnType", bound="Annotation")


class Annotation:
    """Class holding the annotation information.

    Attributes:
        coord: Tuple holding the seqname as well as start and end position
            of the feature, with sequence numbering starting at 0 and being half open.
        seqname: Name of the chromosome or scaffold.
        source: Name of the program that generated this feature, or the data source.
        feature: Feature type name.
        score: A floating point value.
        strand: Defined as + (forward) or - (reverse).
        frame: Position of the first base of the feature relative to the first base of
            the codon.
        attribute: A dictionary holding tag-value pairs, roviding additional information
            about each feature.

    """

    # pylint: disable=too-many-arguments
    def __init__(
        self,
        coord: Tuple[str, int, int],
        source: str,
        feature: str,
        score: str,
        strand: str,
        frame: str,
        attribute: Dict[str, str],
    ) -> None:
        """Initialize the class with all the fields from the single line
        of GTF file.
        """
        self.coord = coord
        self.source = source
        self.feature = feature
        self.score = score
        self.strand = strand
        self.frame = frame
        self.attribute = attribute

    # pylint: enable=too-many-arguments

    def __repr__(self) -> str:
        attr_str: str = "; ".join(
            [
                " ".join([pair[0], '"' + pair[1] + '"'])
                for pair in self.attribute.items()
            ]
        ) + ";"
        return "\t".join(
            [
                self.coord[0],
                self.source,
                self.feature,
                str(self.coord[1] + 1),
                str(self.coord[2]),
                self.score,
                self.strand,
                self.frame,
                attr_str,
            ]
        )

    @classmethod
    def from_gtf(cls: Type[AnnType], gtf_line: str) -> AnnType:
        """Generate dictionary from a single line of a GTF file.

        Note:
            Parses Ensembl compatible GTF files containing 9 columns of data.
            See `GFF/GTF File Format Definition
            <https://www.ensembl.org/info/website/upload/gff.html>`_.

        Args:
            gtf_line: A line from the GTF genome annotation file.

        Returns:
            Object of the Annotaion class.

        """
        content: List[str] = gtf_line.rstrip("\n").split("\t")
        attribute: Dict[str, str] = {}
        if len(content) == 9:
            for pair in content[8].rstrip(";").split(";"):
                key, value = pair.lstrip().split(maxsplit=1)
                attribute[key] = value.strip('"')
        return cls(
            coord=(content[0], int(content[3]) - 1, int(content[4])),
            source=content[1],
            feature=content[2],
            score=content[5],
            strand=content[6],
            frame=content[7],
            attribute=attribute,
        )

    def filter_by_attribute(self, attr: str, members: Iterable[str]) -> bool:
        """Filter the annotation by matching attribute string."""
        if self.attribute[attr] in members:
            return True
        return False

    def filter_by_coord(
        self, location: Tuple[str, Optional[int], Optional[int]]
    ) -> bool:
        """Filter the annotation by coords.

        Inclusive filter.

        Note:
            It does currently not care about preserving the complete gene annotation
        """
        if self.coord[0] != location[0]:
            return False
        if isinstance(location[1], int) and self.coord[1] < location[1]:
            return False
        if isinstance(location[2], int) and self.coord[2] > location[2]:
            return False
        return True

    @staticmethod
    def read_gtf(
        open_gtf: TextIO,
        attr_filter: Optional[Iterable[Tuple[str, Iterable[str]]]] = None,
        coord_filter: Optional[Iterable[Tuple[str, int, int]]] = None,
    ) -> Iterable["Annotation"]:
        """Process the opened gtf file and yields :class:`Annotation <Annotation>`
        optionally matching the specified filters.
        """
        # if the attribute filter was passed turn the parameters into set for faster
        # membership testing
        if attr_filter:
            attr_set: List[Tuple[str, Set[str]]] = list(
                map(lambda x: (x[0], set(x[1])), attr_filter)
            )
        logging.debug("Starting file processing.")
        for line in open_gtf:
            if not line.startswith("#!"):
                filtered: bool = True
                annotation: Annotation = Annotation.from_gtf(line)
                # apply the attribute filter if it was passed
                if attr_filter:
                    for attribute, members_set in attr_set:
                        # if any of the attributes does not match the filter stop
                        # checking and filter out this annotation
                        if not annotation.filter_by_attribute(attribute, members_set):
                            filtered = False
                            break
                # apply the coord filter if it was passed, but only if the attribute
                # filter was succesfull
                if coord_filter and filtered:
                    for coords in coord_filter:
                        # if any coords fit, stop checking
                        if annotation.filter_by_coord(coords):
                            filtered = True
                            break
                        filtered = False
                if filtered:
                    yield annotation
        logging.debug("Finished file processing.")


if __name__ == "__main__":
    def handle_gzip(path):
        if path.endswith(".gz"):
            return gzip.open(path, "rt")
        if path == "-":
            return sys.stdin
        return open(path, "r")


    parser = argparse.ArgumentParser(
        description="Extract features in BED6 format from the GTF file."
    )
    parser.add_argument("gtf", type=handle_gzip, default=sys.stdin,
                        help="Input file in Ensembl GTF format.")
    parser.add_argument("--type",
                        "-t",
                        type=str,
                        choices=["gene", "transcript"],
                        default="gene",
                        help="Feature type to extract",
                        )
    parser.add_argument("--verbose",
                        "-v",
                        action="store_true",
                        help="Be verbose."
                        )
    args = parser.parse_args()

    log_level = logging.ERROR
    if args.verbose:
        log_level = logging.DEBUG
    logger_format = (
        "%(asctime)s:%(levelname)s:%(name)s:%(threadName)s(%(process)s):%(message)s"
    )
    logging.basicConfig(format=logger_format, level=log_level, datefmt="%H:%M:%S")

    with args.gtf as gtf_file:
        id_field = "gene_id"
        if args.type == "transcript":
            id_field = "transcript_id"
        results: Iterable[Annotation] = Annotation.read_gtf(gtf_file)
        for ann in results:
            if ann.feature == args.type:
                print(ann.coord[0],
                      ann.coord[1],
                      ann.coord[2],
                      ann.attribute[id_field],
                      "0",
                      ann.strand,
                      sep="\t",
                      file=sys.stdout,
                      )
