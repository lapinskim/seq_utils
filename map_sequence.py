#!/usr/bin/env python3
# coding: utf-8
"""Map restriction enzyme sequence on the reference genome."""

import argparse
import gzip
import logging
from typing import Iterator, List, Tuple, TextIO
import statistics
import sys


def sequence_search(sequence:str, reference:str, cut_site: int) -> Iterator[int]:
    logging.debug("Starting sequence search.")
    index:int = 0
    while index != -1:
        index = reference.find(sequence, index)
        if index != -1:
            cut_pos:int = index + cut_site
            if cut_pos < 0:
                cut_pos = 0
            yield cut_pos
            index += 1


def assemble_reference(reference_file: TextIO) -> Iterator[Tuple[str, str]]:
    line: str
    contig_name: str = ""
    reference: str = ""
    for line in reference_file:
        if line.startswith(">"):
            if reference:
                yield contig_name, reference
            contig_name = line.split(maxsplit=1)[0].strip(">")
            reference = ""
            logging.debug("Assembling sequence for contig " + contig_name + ".")
            continue
        reference += line.rstrip("\n").casefold()
    if reference:
        yield contig_name, reference


def map2reference(sequence: str, cut_site: int, reference_file: TextIO) -> Iterator[Tuple[str, int, int]]:
    sequence = sequence.casefold()
    for name, ref_sequence in assemble_reference(reference_file):
        contig_len:int = len(ref_sequence)
        for cut_pos in sequence_search(sequence, ref_sequence, cut_site):
            yield name, contig_len, cut_pos


def log_stats(lengths_list: List[int], cut_n: int) -> None:
    fragment_n: int = len(lengths_list)
    mean_len: int = statistics.mean(lengths_list)
    stdev_len: int = statistics.stdev(lengths_list)
    median_len: int = statistics.median(lengths_list)
    logging.info("Number of sites: %d\nNumber of fragments: %d\nMean fragment length: %d\nFragment length stdev: %d\nMedian fragment length: %d" %
                 (cut_n, fragment_n, mean_len, stdev_len, median_len))


def generate_bed(sequence: str,
                 cut_site: int,
                 reference_file: TextIO,
                 regions: bool = False,
                 verbose: int = 0,
                 ) -> Iterator[Tuple[str, int, int]]:
    previous_name: str = ""
    previous_cut: int = -1
    previous_len = 0
    if verbose:
        cut_n: int = 0
        fragment_lengths: List[int] = []
    for contig_name, contig_len, cut_pos in map2reference(sequence,
                                                          cut_site,
                                                          reference_file,
                                                          ):
        if not regions:
            yield contig_name, cut_pos, cut_pos + 1
        if contig_name != previous_name:
            if previous_len:
                if verbose:
                    fragment_lengths.append(previous_len - (previous_cut + 1))
                if regions:
                    yield previous_name, previous_cut + 1, previous_len
            previous_cut = -1
            previous_name = contig_name
            previous_len = contig_len
        if verbose:
            cut_n += 1
            fragment_lengths.append(cut_pos - previous_cut)
        if regions:
            yield contig_name, previous_cut + 1, cut_pos + 1
        previous_cut = cut_pos
    if previous_len:
        if verbose:
            fragment_lengths.append(previous_len - (previous_cut + 1))
        if regions:
            yield previous_name, previous_cut + 1, previous_len
    if verbose:
        log_stats(fragment_lengths, cut_n)


if __name__ == "__main__":
    def handle_gzip(path):
        if path.endswith(".gz"):
            return gzip.open(path, "rt")
        if path == "-":
            return sys.stdin
        return open(path, "r")


    parser = argparse.ArgumentParser(
        description="Map restriction enzyme sequence on the reference genome."
    )
    parser.add_argument("fasta", type=handle_gzip, default=sys.stdin,
                        help="Input file in FASTA format.")
    parser.add_argument("--sequence",
                        "-s",
                        type=str,
                        required=True,
                        help="Restriction site to map.",
                        )
    parser.add_argument("--position",
                        "-p",
                        type=int,
                        default=0,
                        help="Position of the cut site within the recognized sequence, 0-based.",
                        )
    parser.add_argument("--regions",
                        "-r",
                        action="store_true",
                        help="Output restriction fragments instead of cut position in BED3 format.",
                        )
    parser.add_argument("--verbose",
                        "-v",
                        action="count",
                        default=0,
                        help="Display simple statistics. Passing second argument outputs debug info.",
                        )
    args = parser.parse_args()

    log_level = logging.ERROR
    logger_format = (
        "%(asctime)s:%(levelname)s:%(name)s:%(threadName)s(%(process)s):%(message)s"
    )
    if args.verbose == 1:
        log_level = logging.INFO
        logger_format = "%(message)s"
    elif args.verbose > 1:
        log_level = logging.DEBUG
    logging.basicConfig(format=logger_format, level=log_level, datefmt="%H:%M:%S")

    with args.fasta as fasta_file:
        for coords in generate_bed(args.sequence,
                                   args.position,
                                   fasta_file,
                                   args.regions,
                                   args.verbose,
                                   ):
            print("\t".join(str(e) for e in coords), file=sys.stdout)
