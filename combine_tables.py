#!/usr/bin/env python3

import argparse
import os
import pandas as pd
import sys


def read_file(path, sep="\t", header=None):
    name = os.path.basename(path).split(".")[0]
    s = pd.DataFrame([name], index=[1], columns=["Sample Name"]).T
    df = pd.read_csv(path, sep=sep, header=header, index_col=0)
    return pd.concat([s, df])


def main(files, output, sep="\t", header=None):
    tables = map(lambda x: read_file(x, sep=sep, header=header), files)
    pd.concat(tables, axis=1).T.to_csv(
        output,
        sep=sep,
        index=False,
    )


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description="Combine multiple tables and write transposed result"
    )
    parser.add_argument(
        "tables",
        type=str,
        nargs="+",
    )
    parser.add_argument(
        "-o",
        "--output",
        default=sys.stdout,
    )
    parser.add_argument(
        "-s",
        "--separator",
        default="\t",
        type=str,
    )
    parser.add_argument(
        "--header",
        default=None,
        type=int,
    )
    args = parser.parse_args()

    main(args.tables, args.output, args.separator, args.header)
