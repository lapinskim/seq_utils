#!/usr/bin/env python3

import argparse
import pandas as pd
import re
import sys


def read_fasta(path: str):
    with open(path, "r") as handle:
        for line in handle:
            if line.startswith(">"):
                yield line.lstrip(">").rstrip("\n")


def line_to_dict(line: str, regexp: re.Pattern):
    line_list = line.split(maxsplit=2)
    result = {"seqname": line_list[0]}
    for k, v in regexp.findall(line_list[2]):
        result[k] = v
    return result


def annotate_idxstats(idxstats_path, fasta_path):
    pattern = r"([^\s]+?):(.+?)(?=\s[^\[;\s]+?:|\s*?$)"
    prog = re.compile(pattern)
    headers = pd.DataFrame(map(lambda x: line_to_dict(x, prog),
                               read_fasta(fasta_path))).set_index("seqname")

    idxstats = pd.read_table(
        idxstats_path,
        header=None,
        names=[
            "seqname",
            "length",
            "mapped",
            "unmapped",
        ]
    ).set_index("seqname")

    joined = idxstats.join(headers)

    return joined


def main(feature, idxstats_path, fasta_path, output, sep="\t"):
    joined = annotate_idxstats(idxstats_path, fasta_path)
    joined.loc["*", "mapped"] = joined.loc["*", "unmapped"]
    joined.loc["*", feature] = "unmapped"
    joined[feature].fillna("no_feature", inplace=True)

    joined.groupby(feature)["mapped"].sum().to_csv(
        output,
        sep=sep,
        header=None,
    )


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description="""
Annotate IDXSTATS counts with features from FASTA headers.
Group by GROUPBY and return the sum of mapped and unmapped alignments.
"""
    )
    parser.add_argument(
        "-g",
        "--groupby",
        type=str,
        default="gene_biotype",
        help="Feature to group by.",
    )
    parser.add_argument(
        "-i",
        "--idxstats",
        type=str,
        required=True,
        help="Idxstats file path.",
    )
    parser.add_argument(
        "-f",
        "--fasta",
        type=str,
        required=True,
        help="Fasta file path."
    )
    parser.add_argument(
        "-o",
        "--output",
        default=sys.stdout,
        help="Default STANDARD OUT."
    )
    parser.add_argument(
        "-s",
        "--separator",
        default="\t",
        type=str,
    )
    args = parser.parse_args()

    main(args.groupby, args.idxstats, args.fasta, args.output, args.separator)
