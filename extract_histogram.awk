#!/usr/bin/env -S awk -f

BEGIN {
    # first line of histogram data
    start=12;
    # by default consider the all_set column
    # for library complexity estimation
    column=3;
    # pass the "all=0" value to the script to use the non_optical_sets column
    if (!all) {
        all=1;
    }
    if (all == 0) {
        column=3;
    }
}
{
    if (NF && NR>=start) {
        bin=$1;
        sub(/\.0/,"", bin);
        counts = $(column);
        # do not print bins with 0 counts
        if (counts != 0) {
            print bin, counts;
        }
    }
}
